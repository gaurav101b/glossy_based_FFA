/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */


/**
 * \file
 *         Glossy core, source file.
 * \author
 * 		   Sudipta Saha <sudipta.saha.22@gmail.com>
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#include "glossy.h"
#include "node-id.h"
#include "packetsync.h"

static volatile uint8_t state_tdma;
static volatile uint8_t stage_tdma;

uint8_t new_node_id;
uint8_t max_num_schedule;
uint8_t start_node_id;
uint8_t start_node_index;
uint8_t end_node_id;
uint8_t end_node_index;
uint8_t recev_packet_type, packet_type_trans;
uint8_t length_segment;
uint8_t length_req_packet;

uint8_t relay_count;
uint8_t schedule_number;
uint8_t temp_schedule_number;
uint8_t max_iteration_per_schedule;
uint8_t hop_count;
uint8_t target_hop;
uint8_t from_hop;

uint8_t temp_hop_count;
uint8_t temp_initiator_id;

unsigned int received_but_not_shared;
uint8_t test=0;
unsigned int n_f_strans,start,tx_INIT, rx_INIT, rx_REP_HDR, rx_REP_BDY;
unsigned int rx_REP_TRL, mx_REP_TRL, tx_REP_HDR, tx_REP_BDY, tx_REP_TRL;

/*--------------------------------------------------------------------------------------
We shall use this array for indicating
which of the sub-slots a certain source node
has data to transmit.
*/
static uint8_t packetsync_slots[MAX_ITEMS];
static uint8_t packetsync_data[MAX_ITEMS];

#if TYPE_OF_SOURCE_IDS == RANDOM_SOURCE_IDS
static uint8_t *source_ids;
#elif TYPE_OF_SOURCE_IDS == FIXED_SOURCE_IDS
//static uint8_t source_ids[] = {3 ,4 ,5 ,7 ,9 ,10,11,12,13,14,15,17,31,49,54,55,56,58,61,62,63,72,73};
//static uint8_t source_ids[] = {3,4,5,7,9,10,11,12,13,14,15,17,31,49,54,55,56,58,61,62,63,72,73};
//static uint8_t source_ids[] = {2 ,6 ,8 ,16,18,19,20,21,22,23,24,25,27,28,29,32,33,34,35,37,39,40,41};
//static uint8_t source_ids[] = {2 ,6 ,8 ,16,18,19,20,21,22,23,24,25,27,28,29,32,33,34,35,37,39,40,41},44,47,50,52,53,58,59,60,63,64,65,66,67,70,71,73,74,75,76,77,78,82,85,87,88,89,90,92,94};
static uint8_t source_ids[] = {2,3};
#endif

uint8_t packetsync_itr_cnt;
uint8_t packetsync_schd_cnt;

volatile uint8_t state_packetsync;
volatile uint8_t stage_packetsync;

static uint8_t relay_counts_received[20];
static uint8_t num_relay_count_received;

static uint8_t packetsync_initiator_id;
static uint8_t packetsync_initiator, sync, packetsync_rx_cnt, packetsync_tx_cnt, packetsync_tx_max;
static uint8_t *packetsync_packet, *packetsync_recev_packet;
static uint8_t packetsync_data_len, packetsync_packet_len, packetsync_packet_len_tmp, packetsync_header;
static uint8_t packetsync_bytes_read, packetsync_tx_relay_cnt_last, packetsync_n_timeouts;

static rtimer_clock_t packetsync_t_rx_start, packetsync_t_rx_stop, packetsync_t_tx_start, packetsync_t_tx_stop, packetsync_t_start;
static rtimer_clock_t packetsync_t_rx_timeout;
static rtimer_clock_t T_irq;
static rtimer_clock_t packetsync_t_stop;
static rtimer_callback_t packetsync_cb;
static struct rtimer *packetsync_rtimer;
static void *packetsync_ptr;

static rtimer_clock_t T_slot_h, T_rx_h, T_w_rt_h, T_tx_h, T_w_tr_h, t_ref_l, T_offset_h, t_first_rx_l;


#if TYPE_OF_SOURCE_IDS == RANDOM_SOURCE_IDS ||\
	TYPE_OF_SOURCE_IDS == FIXED_SOURCE_IDS

void fill_random_source_ids(){

	uint8_t source_index=0;

	while(source_index<TDMA_NUM_SLOT){

		uint8_t random_value = (uint8_t)(rand());
		random_value = ((random_value)%98)+1;
		uint8_t j = 0;
		uint8_t flag = 0;
		for (;j<source_index;j++){
			// Check from 0 to source_index-1
			if(source_ids[j]==random_value){
				flag = 1;
				break;
			}
		}

		// Check for repeate random number generation
		if(flag==0){
			source_ids[source_index] = (uint8_t)random_value;
			source_index++;
		}
	}
}


static inline uint8_t get_node_index(){

	uint8_t index=0;
	for(;index<TDMA_NUM_SLOT;index++){
		if(source_ids[index]==new_node_id){
			return index;
		}
	}
	return index;

}
#endif

static inline void initialize_slots(){

	uint8_t i;
	for(i=0;i<MAX_ITEMS;i++)
		*(packetsync_slots+i)=*(packetsync_data+i)=0;

}

static inline void initialize_source_data(){

	uint8_t position = 0;

#if TYPE_OF_SOURCE_IDS == RANDOM_SOURCE_IDS \
	|| TYPE_OF_SOURCE_IDS == FIXED_SOURCE_IDS

	position = get_node_index() + 1;
	if(position>=1 && position<=TDMA_NUM_SLOT){
		packetsync_slots[start_node_index+position-1] = 1;
		packetsync_data[start_node_index+position-1] = new_node_id;	// Put the data here - that you want to share
	}

#elif TYPE_OF_SOURCE_IDS == DEFAULT_SOURCE_IDS

	start_node_id = TDMA_START_NODE_ID;
	end_node_id = start_node_id + TDMA_NUM_SLOT - 1;
	start_node_index = start_node_id - 1;		// Just add this one whenever you try to access slots or tdma_data
	if(new_node_id >= start_node_id && new_node_id<=end_node_id){
		position = new_node_id - start_node_id + 1;
		slots[start_node_index+position-1] = 1;
		tdma_data[start_node_index+position-1] = new_node_id;	// Put the data here - that you want to share
	}

#elif TYPE_OF_SOURCE_IDS == SCHEDULE_BASED_SOURCE_IDS

	if(schedule_number!=255){

		start_node_id = ((schedule_number*(uint8_t)TDMA_NUM_SLOT)+1);//TDMA_START_NODE_ID;
		end_node_id = start_node_id + TDMA_NUM_SLOT - 1;
		start_node_index = start_node_id - 1;		// Just add this one whenever you try to
													// access slots or tdma_data

		if(new_node_id >= start_node_id && new_node_id<=end_node_id){
			position = new_node_id - start_node_id + 1;
			packetsync_slots[start_node_index+position-1] = 1;
			packetsync_data[start_node_index+position-1] = new_node_id;	// Put the data here - that you want to share
		}

	}

#endif

}

static inline void initialize_packets_buffers(){

	uint8_t i;
	for(i=0;i<128;i++)
		*(packetsync_packet+i)=*(packetsync_recev_packet+i)=0;

}

inline static void prepare_INIT(){

	packet_type_trans 			= TDMA_PACKET_TYPE_INIT;
	TDMA_TRANS_PACKET_LEN 		= (TDMA_PACKET_SIZE-1);		// The length field
	TDMA_TRANS_PACKET_TYPE		= MERGE_TYPE_SCHEDULE(packet_type_trans, schedule_number);
	//TDMA_TRANS_PACKET_TYPE 	= packet_type_trans;	// packet type
	TDMA_TRANS_SCHEDULE_ID 		= relay_count;//max_iteration_per_schedule;			// schedule number, in the header packets
	TDMA_TRANS_DATA				= 0;//hop_count;
	packetsync_packet_len_tmp 	= TDMA_TRANS_PACKET_LEN;

}

inline static void prepare_REP_HEADER(){

	packet_type_trans 			= TDMA_PACKET_TYPE_REP_HEADER;
	TDMA_TRANS_PACKET_LEN 		= (TDMA_PACKET_SIZE-1);			// The length field
	TDMA_TRANS_PACKET_TYPE		= MERGE_TYPE_SCHEDULE(packet_type_trans, schedule_number);
	//TDMA_TRANS_PACKET_TYPE 	= packet_type_trans;	// packet type
	TDMA_TRANS_SCHEDULE_ID 		= 0;//max_iteration_per_schedule;
	// schedule number, in the header packets
	TDMA_TRANS_DATA				= 0;//hop_count;
	packetsync_packet_len_tmp 	= TDMA_TRANS_PACKET_LEN;
}

inline static void prepare_REP_BODY(){

	packet_type_trans 			= TDMA_PACKET_TYPE_REP_BODY;
	TDMA_TRANS_PACKET_LEN 		= (TDMA_PACKET_SIZE-1);		// The length field
	TDMA_TRANS_PACKET_TYPE		= MERGE_TYPE_SCHEDULE(packet_type_trans, schedule_number);
	//TDMA_TRANS_PACKET_TYPE 	= packet_type_trans;	// packet type

	// If we have data for this sub-slot - we set the value as the
	// position of the owner of this slot (+1).
	// Otherwise we set 255 to indicate invalid.

	if(packetsync_slots[start_node_index+ packetsync_tx_cnt-1]==1){
		TDMA_TRANS_SCHEDULE_ID 	= (packetsync_tx_cnt);
	} else {
		TDMA_TRANS_SCHEDULE_ID 	= 255;
	}

	TDMA_TRANS_DATA				= packetsync_data[start_node_index+packetsync_tx_cnt-1]; //(uint8_t)node_id;//tdma_tx_cnt;		// The data is also the node id
	packetsync_packet_len_tmp 	= TDMA_TRANS_PACKET_LEN;

}

inline static void prepare_REP_TRAILER(){

	packet_type_trans 			= TDMA_PACKET_TYPE_REP_TRAILER;
	TDMA_TRANS_PACKET_LEN 		= (TDMA_PACKET_SIZE-1);		// The length field
	TDMA_TRANS_PACKET_TYPE		= MERGE_TYPE_SCHEDULE(packet_type_trans, schedule_number);
	//TDMA_TRANS_PACKET_TYPE 	= packet_type_trans;	// packet type
	TDMA_TRANS_SCHEDULE_ID 		= 0;//max_iteration_per_schedule;			// schedule number, in the header packets
	TDMA_TRANS_DATA				= 0;//hop_count;
	packetsync_packet_len_tmp 				= TDMA_TRANS_PACKET_LEN;
}



inline void packetsync_begin_tx(){

	// Depending on what exactly we started to transmit
	// We need to change the state.

	if(packet_type_trans==TDMA_PACKET_TYPE_INIT){


		state_tdma = STATE_TDMA_TRANSMITTING_INIT;
		state_radio = STATE_RADIO_TRANSMITTING_NORMAL;

	} else {

		if(packet_type_trans==TDMA_PACKET_TYPE_REP_HEADER){

			tx_REP_HDR++;

#if CHECK_UPTO_FIRST_REP_HDR_TRANSMIT
			state_tdma = STATE_TDMA_TRANSMITTING_REP_HEADER;
			state_radio = STATE_RADIO_TRANSMITTING_NORMAL;
			return;

#endif
			state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
			state_tdma = STATE_TDMA_TRANSMITTING_REP_HEADER;
			stage_tdma = STAGE_TDMA_TRANSMITTER;

		} else {

			if(packet_type_trans==TDMA_PACKET_TYPE_REP_BODY){

				test++;
				tx_REP_BDY++;
				state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
				state_tdma = STATE_TDMA_TRANSMITTING_REP_BODY;
				stage_tdma = STAGE_TDMA_TRANSMITTER;

			} else {

				if(packet_type_trans==TDMA_PACKET_TYPE_REP_TRAILER){

					//tx_REP_TRL++;
					state_tdma = STATE_TDMA_TRANSMITTING_REP_TRAILER;

				}
			}
		}
	}
}

inline void packetsync_begin_rx(){

	packetsync_bytes_read = 0;
	state_radio = STATE_RADIO_RECEIVING_NORMAL;
//	t_rx_timeout = t_rx_start + ((rtimer_clock_t)(TDMA_PACKET_SIZE)* 35 + 200) * 4;	// For DCO
	packetsync_t_rx_timeout = packetsync_t_rx_start + 1640;	// To save the time to multiply

	uint8_t len;
	// Read Length
	//READ_BYTE(TDMA_RECEV_PACKET_LEN);bytes_read++;
	READ_BYTE(len);packetsync_bytes_read++;

	if(IS_LENGTH_OKAY(len)){

		uint8_t temp_type;
		packetsync_recev_packet[0]=len;
		// Read packet type
		//READ_BYTE(TDMA_RECEV_PACKET_TYPE);bytes_read++;
		READ_BYTE(temp_type);packetsync_bytes_read++;
		packetsync_recev_packet[1]=temp_type;
		//packet_type_recev = TDMA_RECEV_PACKET_TYPE;
		recev_packet_type = GET_PACKET_TYPE(temp_type);
		temp_schedule_number = GET_SCHEDULE_NUMBER(temp_type);

		if(recev_packet_type == TDMA_PACKET_TYPE_INIT){

			// Whenever we get INIT we always prepare to transmit
			// We donot need to think whether we do or not.
			// Since INITs are not forwarded - this is only for the
			// first hop nodes

#if	CHECK_UPTO_INIT_TRANSMIT
			state_radio = STATE_RADIO_RECEIVING_NORMAL;
			stage_tdma = STAGE_TDMA_RECEIVER;
			return;
#endif
			state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;
			stage_tdma = STAGE_TDMA_RECEIVER;

			// Initialize the transmission counter
			packetsync_tx_cnt = 0;

		} else {

			if(recev_packet_type == TDMA_PACKET_TYPE_REP_HEADER){

				packetsync_stop_initiator_timeout();

				stage_tdma = STAGE_TDMA_RECEIVER;
				state_radio = STATE_RADIO_RECEIVING_NORMAL;
				if(state_tdma == STATE_TDMA_EXPECTING_REP_BODY){
					// it was expecting either TRAILER or another BODY packet
					// But suddenly it received a HEADER packet -
					// This implies that - if it received a HEADER ealer -
					// - there was an ongoing process ...
					//state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

					mx_REP_TRL++;
				}

			} else {

				if(recev_packet_type == TDMA_PACKET_TYPE_REP_BODY){

					state_radio = STATE_RADIO_RECEIVING_NORMAL;

					// Note: There can be another case:
					// If we were expecting BODY but it actually missed the
					// TRAILER and then also the HEADER - and the received BODY
					// is in the series in which it itself should have participated
					// Chance is rare though - it may happen. Take care.

					// Difficult - since a BODY packet then should contain an
					// iteration count. The tdma_itr_count should be included in the
					// Body as well as HEADER as well as TRAILER.

					// No harm of corruption:
					// We shall have to bring another value which will be like the
					// relay_count in Glossy. This relay_count value will indicate
					// the number of times the series of packets are begin transmitted
					// Initiator will make it 0, first layer node will make it 1,
					// when the second layer node gets it will make it 2 - whenever a node
					// will get a value - before starting its own transmission - it will
					// increment the value by 1 and then transmit in tw own series.
					// The Hop count is basically the first value a node receives of this variable.

				} else {

					if(recev_packet_type == TDMA_PACKET_TYPE_REP_TRAILER){

#if CHECK_UPTO_REP_TRL_TRANSMIT
						packetsync_tx_cnt = 0;
						state_radio = STATE_RADIO_RECEIVING_NORMAL;
						return;
#endif
						packetsync_tx_cnt = 0;
						state_radio = STATE_RADIO_RECEIVING_FOR_SYNCH;

					} else {

						radio_abort_rx();
						state_radio = STATE_RADIO_WAITING;

					}
				}
			}
		}

	} else {

		radio_abort_rx();
		state_radio = STATE_RADIO_WAITING;
	}

	glossy_schedule_rx_timeout(packetsync_t_rx_timeout);

}

inline void packetsync_end_tx_sync(){

	// The previous transmission just ends here
	packetsync_tx_cnt++;
	radio_flush_tx();



	if(packetsync_tx_cnt== TDMA_NUM_SLOT+1){

		// Raise the power level to the highest value
		fast_set_power(31);
		prepare_REP_TRAILER();

	} else {

		// Set the power level to a value
		// According to the slots array

		if(packetsync_slots[start_node_index+packetsync_tx_cnt-1]==1)
			fast_set_power(31);
		else
			fast_set_power(0);

		// This will put the value of tdma_tx_cnt and (sender)
		// the content of tdma_data in the packet (data)
		prepare_REP_BODY();
	}

	radio_write_tx(packetsync_packet, packetsync_packet_len_tmp);
	state_radio = STATE_RADIO_TRANSMITTING_NORMAL;

}

inline void packetsync_end_rx_sync(){

	// Packet length and packet type i.e. 2 bytes have been read already
	// Rest number of bytes is TDMA_PACKET_SIZE-2
	//FASTSPI_READ_FIFO_NO_WAIT(&recev_packet[bytes_read], packet_len_tmp - bytes_read + 1);
	FASTSPI_READ_FIFO_NO_WAIT(&packetsync_recev_packet[packetsync_bytes_read], 4);
	//	READ_BYTE(TDMA_RECEV_SCHEDULE_ID);	bytes_read++;
	//	READ_BYTE(TDMA_RECEV_DATA);			bytes_read++;
	//	READ_BYTE(TDMA_RECEV_RSSI);			bytes_read++;
	//	READ_BYTE(TDMA_RECEV_CRC);			bytes_read++;
	radio_flush_rx();

	if(TDMA_RECEV_CRC & FOOTER1_CRC_OK){

		if(recev_packet_type == TDMA_PACKET_TYPE_INIT){

			relay_count = TDMA_RECEV_SCHEDULE_ID;
			rx_INIT++;
			if(hop_count == 255) {
				// Hop count is not yet set so set it here
				hop_count = TDMA_RECEV_DATA + 1;
			}

			// When you get an INIT if you are not INITIATOR
			// And if you get a different schedule_number
			// Then set.
			if(!packetsync_initiator && (schedule_number != temp_schedule_number)){

				//max_iteration_per_schedule = (TDMA_RECEV_SCHEDULE_ID);
				schedule_number = temp_schedule_number;
				initialize_source_data();

			}

//			if(relay_count == MAX_RELAY_COUNT-1){
				prepare_REP_HEADER();
				radio_write_tx(packetsync_packet, packetsync_packet_len_tmp);
//			}else {
//				relay_count++;
//				radio_write_tx();
//				prepare_INIT();
//			}

		} else {

			if(recev_packet_type == TDMA_PACKET_TYPE_REP_TRAILER){

				rx_REP_TRL++;

				if(hop_count == 255) {
					// Hop count is not yet set so set it here
					hop_count = TDMA_RECEV_DATA + 1;
				}

				if(!packetsync_initiator && schedule_number != temp_schedule_number){

					//max_iteration_per_schedule = (TDMA_RECEV_SCHEDULE_ID);
					schedule_number = temp_schedule_number;
					initialize_source_data();

				}

				// If a node receives a TRAILER
				// Then it will check whether it has already transmitted
				// max times. If so, then it will stop.
				// These will be mostly the OTHER nodes (not INITIATOR)

				if(packetsync_itr_cnt==max_iteration_per_schedule){

					state_tdma = STATE_TDMA_OFF;
					state_radio = STATE_RADIO_WAITING;
					radio_off();

				} else {

					// This is after reception when you have
					// already fired transmission ...
					// Now you need to write proper thing in the packet
					// OR turn off radio.

					// Note that a non-initiator will also come here
					// But an initiator will come here first with this
					// Condition tdma_itr_cnt = MAX

					prepare_REP_HEADER();
					radio_write_tx(packetsync_packet, packetsync_packet_len_tmp);
				}

			} else {

				/*--------------------------------------------------------------------------------------*/
				// This else block is purely in order to handle
				// the case of

				if(recev_packet_type == TDMA_PACKET_TYPE_REP_HEADER){

//					// Here comes means the node has missed the last TRAILER.
//					// So, it will now assume that the header packet has been transmitted
//					// Now, first it will check whether it should participate in
//					// Transmission or not.
//
//
//					if(tdma_itr_cnt==max_iteration_per_schedule){
//
//						// No - have already transmitted MAX times
//						// Just receive and OFF;
//
//						state_tdma = STATE_TDMA_OFF;
//						state_radio = STATE_RADIO_WAITING;
//						radio_off();

//					} else {
//
//						// Yes - so go for transmission
//
//						tdma_tx_cnt=1;	// Assume HEADER already transmitted just
//						state_tdma = STATE_TDMA_TRANSMITTING_REP_BODY;
//
//						if(slots[tdma_tx_cnt]==1)
//							fast_set_power(31);
//						else
//							fast_set_power(0);
//
//						// This will put the value of tdma_tx_cnt and (sender)
//						// the content of tdma_data in the packet (data)
//						prepare_REP_BODY();
//						radio_write_tx();
//
//					}

				}

				/*--------------------------------------------------------------------------------------*/

			}

		}

	} else {

		radio_abort_tx();
		state_radio = STATE_RADIO_WAITING;
	}
}

inline void packetsync_end_rx_normal(){

	glossy_stop_rx_timeout();
	state_radio = STATE_RADIO_WAITING;
	//FASTSPI_READ_FIFO_NO_WAIT(&recev_packet[bytes_read], packet_len_tmp - bytes_read + 1);
	FASTSPI_READ_FIFO_NO_WAIT(&packetsync_recev_packet[packetsync_bytes_read], 4);
	//	READ_BYTE(TDMA_RECEV_SCHEDULE_ID);	bytes_read++;
	//	READ_BYTE(TDMA_RECEV_DATA);			bytes_read++;
	//	READ_BYTE(TDMA_RECEV_RSSI);			bytes_read++;
	//	READ_BYTE(TDMA_RECEV_CRC);			bytes_read++;

	if(TDMA_RECEV_CRC & FOOTER1_CRC_OK){

		if(recev_packet_type == TDMA_PACKET_TYPE_REP_HEADER){

#if	CHECK_UPTO_FIRST_REP_HDR_TRANSMIT


				rx_REP_HDR++;
				radio_off();
				state_tdma = STATE_TDMA_OFF;
				state_radio = STATE_RADIO_WAITING;
				return;

#endif
			rx_REP_HDR++;
			// So that next time when it transmit HEADER the power is 31
			fast_set_power(31);

			// We are first time we are receiving a packet
			// So, we set our hop count
			//

			if(hop_count == 255) {
				// Hop count is not yet set so set it here
				hop_count = TDMA_RECEV_DATA + 1;
			}
			// Reception of Header packet ends here - so we should
			// be expecting body packets now.
			if(!packetsync_initiator && schedule_number != temp_schedule_number){

				//max_iteration_per_schedule = (TDMA_RECEV_SCHEDULE_ID);
				schedule_number = temp_schedule_number;
				initialize_source_data();

			}

			state_tdma = STATE_TDMA_EXPECTING_REP_BODY;

		} else {

			if(recev_packet_type == TDMA_PACKET_TYPE_REP_BODY){

				//-------------------------------------
				// Store the incoming data
				rx_REP_BDY++;
				state_tdma = STATE_TDMA_EXPECTING_REP_BODY;

				// The last one should be expecting TRAILER -
				// But is hard to distinguish using Timer

				//----------------------------------------------------------
				// Check the Schedule id - for good packets it should be
				// a non-zero value >=1 and <=NUM_SLOT and for bad (zero power)
				// packets it will contain 255

				if(TDMA_RECEV_SCHEDULE_ID>=1 && TDMA_RECEV_SCHEDULE_ID<=TDMA_NUM_SLOT){

					// Take this data only if you did not get data
					// for this slot already

					if(packetsync_slots[start_node_index+TDMA_RECEV_SCHEDULE_ID-1]==0){
						packetsync_slots[start_node_index+TDMA_RECEV_SCHEDULE_ID-1] = 1;
						packetsync_data[start_node_index+TDMA_RECEV_SCHEDULE_ID-1] = TDMA_RECEV_DATA;
					}
				}

			} else {

				// Usually the reception of REP trailer will not result
				// in an entry here.
				if(recev_packet_type == TDMA_PACKET_TYPE_REP_TRAILER){


#if CHECK_UPTO_REP_TRL_TRANSMIT

					rx_REP_TRL++;
					state_radio = STATE_RADIO_WAITING;
					state_tdma = STATE_TDMA_OFF;
					radio_off();
					return;

#endif

				} else {

#if CHECK_UPTO_INIT_TRANSMIT

					if(recev_packet_type == TDMA_PACKET_TYPE_INIT){

						rx_INIT++;
						radio_off();
						state_radio = STATE_RADIO_WAITING;
						state_tdma = STATE_TDMA_OFF;
						return;
					}
#endif
				}
			}

		}

		radio_flush_rx();

	} else {

		radio_abort_rx();
		state_radio = STATE_RADIO_WAITING;

	}

}

inline void packetsync_end_tx_normal(){

	ENERGEST_OFF(ENERGEST_TYPE_TRANSMIT);
	ENERGEST_ON(ENERGEST_TYPE_LISTEN);

	radio_flush_tx();
	state_radio = STATE_RADIO_WAITING;

	if(packet_type_trans==TDMA_PACKET_TYPE_INIT){


#if CHECK_UPTO_INIT_TRANSMIT

		tx_INIT++;
		radio_off();
		state_tdma = STATE_TDMA_OFF;
		state_radio = STATE_RADIO_WAITING;
		return;
#endif

		tx_INIT++;
		state_tdma = STATE_TDMA_EXPECTING_REP_HEADER;

	} else {
		if (packet_type_trans==TDMA_PACKET_TYPE_REP_TRAILER) {


#if CHECK_UPTO_REP_TRL_TRANSMIT
			tx_REP_TRL++;
			state_radio = STATE_RADIO_WAITING;
			state_tdma = STATE_TDMA_OFF;
			radio_off();
			return;
#endif



			packetsync_tx_cnt++;
			stage_tdma = STAGE_TDMA_RECEIVER;
			state_tdma = STATE_TDMA_EXPECTING_REP_HEADER;


			if(packetsync_tx_cnt==TDMA_NUM_SLOT+2){

				packetsync_itr_cnt++;

				// If INITIATOR then after transmitting the final
				// INITIATOR will stop
				// But this is not true for the OTHER nodes
				// They will continue to listen - and

				uint8_t v = (node_id == packetsync_initiator_id) ? 1: 0;
				if ((packetsync_itr_cnt == max_iteration_per_schedule) && ((1 - v) == 0)) {

					radio_off();
					state_tdma = STATE_TDMA_OFF;
					state_radio = STATE_RADIO_WAITING;

				}
			}
		}else {

#if	CHECK_UPTO_FIRST_REP_HDR_TRANSMIT

			if(packet_type_trans==TDMA_PACKET_TYPE_REP_HEADER){

				tx_REP_HDR++;
				radio_off();
				state_tdma = STATE_TDMA_OFF;
				state_radio = STATE_RADIO_WAITING;
				return;
			}

#endif
		}
	}
}

/*------------------------------------------------------------------------------------------------*/
inline void state_machine_packetsync(unsigned short packetsync_tbiv) {

	if(state_radio == STATE_RADIO_TRANSMITTING_NORMAL && SFD_IS_1){ 			// Transmission started

		packetsync_t_tx_start = TBCCR1;
		packetsync_begin_tx();

	} else {
		if(state_radio == STATE_RADIO_WAITING && SFD_IS_1){ 					// Reception started

			packetsync_t_rx_start = TBCCR1;
			packetsync_begin_rx();

		} else {
			if(state_radio == STATE_RADIO_RECEIVING_NORMAL && !SFD_IS_1){ 		// Reception finished

				packetsync_t_rx_stop = TBCCR1;
				packetsync_end_rx_normal();

			} else {
				if(state_radio == STATE_RADIO_TRANSMITTING_NORMAL && !SFD_IS_1){// Transmission finished

					packetsync_t_tx_stop = TBCCR1;
					packetsync_end_tx_normal();

				} else {
					if (packetsync_tbiv == TBIV_TBCCR3) {

						// If this timer fires - comes here - then
						// you have not received the data for this particular
						// Subslot - so increase the subslot-counter

					} else {
						if (packetsync_tbiv == TBIV_TBCCR5) {

							// Error handling: Packet reception is stuck
							// Check if we are still trying to receive - then abort it
							// At this time we should be transmitting.
							// This timer has fired means we were stuck -
							// So, we should abort reception and receive the rest packets
							radio_abort_rx();
							state_radio = STATE_RADIO_WAITING;

							glossy_stop_rx_timeout();

							// Transmit after receive happens only after reception of
							// REP_TRAILER or INIT packet.
							// So, when we get stuck in reception and reception does not end normally
							// We should abort the reception
							// In the worst case if we supposed to transmit now - we shall fail to do
						}
					}
				}
			}
		}
	}
}

/* --------------------------- Glossy process ----------------------- */
PROCESS(packetsync_process, "Packetsync busy-waiting process");
PROCESS_THREAD(packetsync_process, ev, data) {
	PROCESS_BEGIN();

	do {

		packetsync_packet = (uint8_t *) malloc(128);
		packetsync_recev_packet = (uint8_t *) malloc(128);

	} while (packetsync_packet == NULL || packetsync_recev_packet == NULL);

	while (1) {
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL);
		// prevent the Contiki main cycle to enter the LPM mode or
		// any other process to run while Glossy is running
		while (PACKETSYNC_IS_ON() && RTIMER_CLOCK_LT(RTIMER_NOW(), packetsync_t_stop));
#if COOJA
		while (get_packetsync_state() != TDMA_STATE_OFF);
#endif /* COOJA */
		// Glossy finished: execute the callback function
		dint();
		packetsync_cb(packetsync_rtimer, packetsync_ptr);
		eint();
	}

	PROCESS_END();
}

void print_relay_counts(void){

	printf("%2u ",relay_counts_received[0]);

}


void packetsync_start(
		uint8_t packetsync_initiator_,
		uint8_t new_node_id_,
		rtimer_callback_t packetsync_cb_,
		struct rtimer *packetsync_rt_,
		void *packetsync_ptr_,
		rtimer_clock_t packetsync_t_stop_)
{

	new_node_id = new_node_id_;
	packetsync_cb = packetsync_cb_;
	packetsync_rtimer = packetsync_rt_;
	packetsync_ptr = packetsync_ptr_;
	packetsync_t_stop = packetsync_t_stop_;

	start = tx_INIT=rx_INIT=rx_REP_HDR=rx_REP_BDY=rx_REP_TRL=mx_REP_TRL=tx_REP_HDR=tx_REP_BDY=tx_REP_TRL=0;

	packetsync_initiator = packetsync_initiator_;

#if TYPE_OF_SOURCE_IDS == RANDOM_SOURCE_IDS
	source_ids = (uint8_t *) malloc(TDMA_NUM_SLOT);
	fill_random_source_ids();
#endif

	initialize_packets_buffers();
	initialize_slots();

	glossy_disable_other_interrupts();


	packetsync_tx_cnt = 0;
	packetsync_itr_cnt = 0;

	if(packetsync_initiator){

		state_tdma = STATE_TDMA_TRANSMITTING_INIT;
		state_radio = STATE_RADIO_TRANSMITTING_NORMAL;
		stage_tdma = STAGE_TDMA_TRANSMITTER;

		//------------------------------------------------------------------------------
		// Ask why this matters
//
//		volatile uint8_t gap;
//		for(gap=0;gap<220;gap++);

		relay_count = 0;
		hop_count			 		= 0;
		packetsync_initiator_id 	= node_id;
		schedule_number 	 		= SCHEDULE_NUMBER;
		temp_schedule_number		= 0;
		max_iteration_per_schedule 	= TDMA_DEFAULT_MAX_ITERATION;

		initialize_source_data();

		radio_flush_rx();
		radio_flush_tx();

		prepare_INIT();

		radio_write_tx(packetsync_packet, packetsync_packet_len_tmp);
		radio_start_tx();
		packetsync_schedule_initiator_timeout();

	} else {

		state_radio = STATE_RADIO_WAITING;
		state_tdma = STATE_TDMA_EXPECTING_INIT;
		stage_tdma = STAGE_TDMA_RECEIVER;

		radio_flush_rx();
		radio_flush_tx();

		relay_count = 255;
		packetsync_initiator_id 	=	255;
		hop_count 					=	255;
		schedule_number 			= 	255;
		temp_schedule_number		= 	255;
		max_iteration_per_schedule 	=	TDMA_DEFAULT_MAX_ITERATION;

		initialize_source_data();
		radio_on();
	}

	process_poll(&packetsync_process);

}

void packetsync_stop(void) {

	packetsync_stop_initiator_timeout();
	radio_off();
	radio_flush_rx();
	radio_flush_tx();
	state_tdma = STATE_TDMA_OFF;
	glossy_enable_other_interrupts();

}


uint8_t get_hop_count(void) {

	return hop_count;

}

uint8_t get_packetsync_rx_cnt(void) {
	return packetsync_rx_cnt;
}



uint8_t get_packetsync_state(void) {
	return state_tdma;
}

uint8_t get_packetsync_stage(void) {
	return stage_tdma;
}

inline void packetsync_end_sync_rx_or_tx(){

	if(stage_tdma == STAGE_TDMA_TRANSMITTER)
		packetsync_end_tx_sync();

	else if(stage_tdma == STAGE_TDMA_RECEIVER)
		packetsync_end_rx_sync();
}


//inline void packetsync_schedule_initiator_timeout(void) {
//#if !COOJA
//	if (sync) {
//		TBCCR4 = packetsync_t_start +
//				(packetsync_n_timeouts + 1) * PACKETSYNC_INITIATOR_TIMEOUT * ((unsigned long)T_slot_h +
//						(packetsync_packet_len * F_CPU) / 31250);
//	} else {
//		TBCCR4 = packetsync_t_start + (packetsync_n_timeouts + 1) * PACKETSYNC_INITIATOR_TIMEOUT *
//				((rtimer_clock_t)packetsync_packet_len * 35 + 400) * 4;
//	}
//	TBCCTL4 = CCIE;
//#endif
//}
//
//inline void packetsync_stop_initiator_timeout(void) {
//	TBCCTL4 = 0;
//}

inline void packetsync_schedule_initiator_timeout(){
	packetsync_t_start = RTIMER_NOW_DCO();
	TBCCR4 = packetsync_t_start + ((unsigned long)((TDMA_PACKET_SIZE + (TDMA_PACKET_SIZE))* F_CPU) / 31250);
	//TBCCR4 = t_start + (((unsigned long)128 + 128/5)* F_CPU) / 31250;
	TBCCTL4 = CCIE;
}

inline void packetsync_stop_initiator_timeout(void) {
	TBCCTL4 = 0;
}

/* ------------------------------ Timeouts -------------------------- */

void print_tdma_data(){

	uint8_t byte;
	byte=0;
	printf("Data received: ");
	for(;byte<TDMA_NUM_SLOT+2;byte++)
		printf("%3u ",packetsync_data[byte]);

}

void print_packet(){

	uint8_t byte;
	byte=0;
	printf("To Trans: ");
	for(;byte<TDMA_PACKET_SIZE;byte++)
		printf("%3u ",packetsync_packet[byte]);

}
void print_recev_packet(){

	uint8_t byte;
	byte=0;
	printf("Received: ");
	for(;byte<TDMA_PACKET_SIZE;byte++)
		printf("%3u ",packetsync_recev_packet[byte]);

}

void print_slot_info(){

	printf("%u, %u ",TDMA_NUM_SLOT, max_iteration_per_schedule);
}


void print_tx_rx(){

//	printf("%2u %2u|%2u %3u %2u %2u|%2u %3u %2u",
//			tx_INIT, rx_INIT, rx_REP_HDR, rx_REP_BDY, rx_REP_TRL,mx_REP_TRL,tx_REP_HDR, tx_REP_BDY, tx_REP_TRL);

//	printf("%2u %2u|%2u %3u %2u %2u|%2u %3u %2u",
	//		tx_INIT, rx_INIT, rx_REP_HDR, rx_REP_BDY, rx_REP_TRL,mx_REP_TRL,tx_REP_HDR, tx_REP_BDY, tx_REP_TRL);
	printf("%2u %2u| %2u %2u| %2u %2u| %2u %2u | %2u",
			tx_INIT, rx_INIT, tx_REP_HDR, rx_REP_HDR, tx_REP_BDY, rx_REP_BDY, tx_REP_TRL, rx_REP_TRL, test);

}

void print_data(){

	uint8_t i;
	uint8_t count=0;
	for(i=0;i<MAX_ITEMS;i++){
		if((*(packetsync_data+i))>0)
			count++;
	}
	printf("%u,%u,%2u,%2u|",schedule_number,hop_count,count,relay_count);

//	printf("Count=%u:",count);
//	for(i=1;i<=TDMA_NUM_SLOT;i++){
//		printf("%2u,",*(tdma_data+i));
//	}

	int lc=0;
	for(i=0;i<MAX_ITEMS;i++){
		if((*(packetsync_data+i))==0){

			// Print only upto 5 ids if missed
			// Donot print - much since Indriya has problem
			// in storing many data in a single line
			// in the database. With sf this is not a problem

			if (lc < 4){
				lc++;
#if TYPE_OF_SOURCE_IDS == RANDOM_SOURCE_IDS
static uint8_t *source_ids;
#elif TYPE_OF_SOURCE_IDS == FIXED_SOURCE_IDS
				printf("%2u,",(source_ids[i-1]));
#elif TYPE_OF_SOURCE_IDS == DEFAULT_SOURCE_IDS
				printf("%2u,",(i-1+TDMA_START_NODE_ID));
#elif TYPE_OF_SOURCE_IDS == SCHEDULE_BASED_SOURCE_IDS
				printf("%2u,",(i+1));
#endif

			}
		}
	}

	while (lc < 4) {
		lc++;
		printf("%2u,",0);
	}

}
void print_source_ids(){

//	int i=0;
//	for(i=0;i<TDMA_NUM_SLOT;i++){
//		printf("%2u,",(source_ids[i]));
//	}

}


//inline void packetsync_schedule_initiator_timeout() {
//#if !COOJA
//	TBCCR4 = app_t_start + (app_n_timeouts + 1) * APP_INITIATOR_TIMEOUT *
//			((rtimer_clock_t)app_packet_len * 35 + 400) * 4;
//
//	TBCCTL4 = CCIE;
//#endif
//}
//
//inline void packetsync_stop_initiator_timeout(void) {
//	TBCCTL4 = 0;
//}



