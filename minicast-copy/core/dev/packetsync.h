/*
 * Copyright (c) 2011, ETH Zurich.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * Author: Federico Ferrari <ferrari@tik.ee.ethz.ch>
 *
 */

/**
 * \file
 *         Glossy core, header file.
 * \author
 *         Federico Ferrari <ferrari@tik.ee.ethz.ch>
 */

#ifndef PACKETSYNC_H_
#define PACKETSYNC_H_

#include "contiki.h"
#include "lib/random.h"
#include "dev/watchdog.h"
#include "cc2420.h"
#include "dev/cc2420_const.h"
#include "dev/leds.h"
#include "dev/spi.h"
//#include "lib/crc16.h"
#include <stdio.h>
#include <legacymsp430.h>
#include <stdlib.h>

/**
 * If not zero, nodes print additional debug information (disabled by default).
 */
#define PACKETSYNC_DEBUG 0
/**
 * Initiator timeout, in number of Glossy slots.
 * When the timeout expires, if the initiator has not received any packet
 * after its first transmission it transmits again.
 */
#define PACKETSYNC_INITIATOR_TIMEOUT      3


#define PACKETSYNC_IS_ON()                (get_packetsync_state() != STATE_TDMA_OFF)

PROCESS_NAME(packetsync_process);

inline void state_machine_packetsync(unsigned short packetsync_tbiv);



#define MERGE_TYPE_SCHEDULE(packet_type, schedule_number) 		(((packet_type<<4)|(schedule_number)))
#define GET_PACKET_TYPE(type_schedule) 							((uint8_t)((type_schedule & 0xf0)>>4) & 0x0f)
#define GET_SCHEDULE_NUMBER(type_schedule) 							((uint8_t)(type_schedule & 0x0f))

#define FIXED_SOURCE_IDS 1			// Given source ids, check the TDMA_NUM_SLOT (should have same value)
#define RANDOM_SOURCE_IDS 2			// Randomly generated each iteration
#define DEFAULT_SOURCE_IDS 3		// Depends on the TDMA_START_NODE_ID
#define SCHEDULE_BASED_SOURCE_IDS 4
//#define TYPE_OF_SOURCE_IDS RANDOM_SOURCE_IDS
//#define TYPE_OF_SOURCE_IDS DEFAULT_SOURCE_IDS
#define TYPE_OF_SOURCE_IDS FIXED_SOURCE_IDS
//#define TYPE_OF_SOURCE_IDS SCHEDULE_BASED_SOURCE_IDS

#define MAX_RELAY_COUNT 1
#define MAX_ITEMS 100
#define TDMA_START_NODE_ID 31
#define TDMA_INITIATOR_ID 1
#define TDMA_PACKET_SIZE 6//(1+1+1+1+2)

#define SCHEDULE_NUMBER 0
#define TDMA_NUM_SLOT 40
#define TDMA_DEFAULT_MAX_ITERATION 1

#define TDMA_IS_ON()                (get_tdma_state() != STATE_TDMA_OFF)

/*-----------------------------------------------------------------------------------------------------------------*/
// Packet structure -
// Every packet is of fixed length which is defined by TDMA_PACKET_SIZE
// There will be three types of packets -
// Header packet - is transmitted by all,
// trailer packet - is transmitted by all
// Body packet - which is transmitted by only the intended node

// First byte is length in every packet
// Header packet 	<one byte length, one byte schedule, one byte for future use, two byte CRC>
// Trailer packet 	<one byte length, one byte schedule, one byte for future use, two byte CRC> - same structure
// Body packet 		<one byte length, one byte schedule, node id, two byte CRC>
/*-----------------------------------------------------------------------------------------------------------------*/

#define TDMA_TRANS_PACKET_LEN 		packetsync_packet[0]			// length field
#define TDMA_TRANS_PACKET_TYPE		packetsync_packet[1]			// schedule number, in the header packets
#define TDMA_TRANS_SCHEDULE_ID 		packetsync_packet[2]			// schedule number, in the header packets
#define TDMA_TRANS_DATA 			packetsync_packet[3]			// schedule number, in data packets

#define TDMA_RECEV_PACKET_LEN 		packetsync_recev_packet[0]			// length field
#define TDMA_RECEV_PACKET_TYPE		packetsync_recev_packet[1]			// schedule number, in the header packets
#define TDMA_RECEV_SCHEDULE_ID 		packetsync_recev_packet[2]			// schedule number, in the header packets
#define TDMA_RECEV_DATA 			packetsync_recev_packet[3]			// schedule number, in data packets
#define TDMA_RECEV_RSSI 			packetsync_recev_packet[4]			// schedule number, in data packets
#define TDMA_RECEV_CRC 				packetsync_recev_packet[5]			// schedule number, in data packets


enum {
	PACKETSYNC_INITIATOR = 1, PACKETSYNC_RECEIVER = 0
};

#define READ_BYTE(PLACE_TO_READ) 				do{																\
													while (!FIFO_IS_1) {										\
														if (!RTIMER_CLOCK_LT(RTIMER_NOW_DCO(), packetsync_t_rx_timeout)) {	\
															radio_flush_rx();									\
															return 0;											\
														}														\
													};															\
													FASTSPI_READ_FIFO_BYTE(PLACE_TO_READ);						\
												}	while(0)



enum state_tdma_packet_type {

	TDMA_PACKET_TYPE_INIT,
	TDMA_PACKET_TYPE_REP_HEADER,
	TDMA_PACKET_TYPE_REP_BODY,
	TDMA_PACKET_TYPE_REP_TRAILER,
};

enum state_tdma_type {

	STATE_TDMA_TRANSMITTING_INIT,
	STATE_TDMA_TRANSMITTING_REP_HEADER,
	STATE_TDMA_TRANSMITTING_REP_BODY,
	STATE_TDMA_TRANSMITTING_REP_TRAILER,

	STATE_TDMA_EXPECTING_INIT,
	STATE_TDMA_EXPECTING_REP_HEADER,
	STATE_TDMA_EXPECTING_REP_BODY,
	STATE_TDMA_EXPECTING_REP_TRAILER,

	STATE_TDMA_OFF,

};

enum stage_tdma_type {

	STAGE_TDMA_TRANSMITTER,
	STAGE_TDMA_RECEIVER,

};

//#define IS_LENGTH_OKAY() 		(TDMA_RECEV_PACKET_LEN == (TDMA_PACKET_SIZE-1))
#define IS_LENGTH_OKAY(len) 		(len == (TDMA_PACKET_SIZE-1))
//---------------------------------------------------------------------------------------------------------


void packetsync_start(
		uint8_t assigned_initiator_id,
		uint8_t new_node_id,
		rtimer_callback_t cb_,
		struct rtimer *rt_,
		void *ptr_,
		rtimer_clock_t t_stop_);

void packetsync_stop(void);


/**
 * \brief            Get the last received counter.
 * \returns          Number of times the packet has been received during
 *                   last Glossy phase.
 *                   If it is zero, the packet was not successfully received.
 */


uint8_t get_initiator_id(void);
void print_tx_rx();

uint8_t get_hop_count(void);

void print_relay_counts(void);

void print_packet();

void print_recev_packet();
/**
 * \brief            Get the current Glossy state.
 * \return           Current Glossy state, one of the possible values
 *                   of \link glossy_state \endlink.
 */

uint8_t get_packetsync_state(void);
uint8_t get_packetsync_stage(void);

inline void packetsync_begin_rx(void);
inline void packetsync_begin_tx(void);

inline void packetsync_end_rx_sync(void);
inline void packetsync_end_tx_sync(void);
inline void packetsync_end_tx_normal(void);
inline void packetsync_end_rx_normal(void);
inline void packetsync_end_sync_rx_or_tx();


#define one_byte_delay_with_copy  			if(already_copied_for_sync<=packet_len_tmp-2){\
												FASTSPI_WRITE_FIFO_RANGE(packet,already_copied_for_sync,already_copied_for_sync+1);\
												already_copied_for_sync++;\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											} else {\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											}
#define one_byte_delay_with_copy_adjusted  	if(already_copied_for_sync<=packet_len_tmp-2){\
												FASTSPI_WRITE_FIFO_RANGE(packet,already_copied_for_sync,already_copied_for_sync+1);\
												already_copied_for_sync++;_NOP();\
											} else {\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
												_NOP();_NOP();_NOP();_NOP();_NOP();\
											}


#define one_byte_delay			 			{_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();}

#define one_byte_with_power_down			{fast_set_power(0);\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();}

#define one_byte_with_power_down_adjusted	{fast_set_power(0);\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();}


#define one_byte_with_power_up				{fast_set_power(MAX_POWER);\
											_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();_NOP();}

// Slot 3 (Low)

#define one_byte_loop_delay	 				{volatile int i; for(i=0;i<12;i++);\
											_NOP();_NOP();_NOP();}

#define one_byte_loop_with_power_down		{volatile int i;fast_set_power(0);\
											for(i=0;i<5;i++); _NOP();_NOP();_NOP();_NOP();}					// 67 clock ticks ; if you measure in example-time-1 it will be +3




#define NOP_DELAY() \
		do {\
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
			_NOP(); _NOP(); _NOP(); _NOP(); \
		} while (0);


#define SPI_SET_TXPOWER(c)\
		do {\
			/* Enable SPI */\
		/*SPI_ENABLE();\*/\
		/* Setting TXCTRL */\
		SPI_TXBUF = CC2420_TXCTRL;\
		NOP_DELAY();\
		SPI_TXBUF = ((u8_t) ((c) >> 8));\
		NOP_DELAY();\
		SPI_TXBUF = ((u8_t) (c));\
		NOP_DELAY();\
		/* Disable SPI */\
		/*SPI_DISABLE();\*/\
		} while (0)

#endif /* PACKETSYNC_H_ */

/** @} */
